// Copyright (c) 2019, Marco Ghibaudi
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. All advertising materials mentioning features or use of this software
//    must display the following acknowledgement:
//    This product includes software developed by Marco Ghibaudi.

// THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

def setup_environment(environment) {  
    // Checking out repository  
    checkout changelog: false, poll: false, scm:   
        [$class: 'SubversionSCM',   additionalCredentials:   [[credentialsId: 'XXX', realm: '']],   
        locations: [[cancelProcessOnExternalsFail: false, credentialsId: XXX, depthOption: 'infinity',    
        ignoreExternalsOption: false,    
        local: 'XXX', remote: 'http://XXX'],    
        [cancelProcessOnExternalsFail: false, credentialsId: 'XXX', depthOption: 'infinity',     
        ignoreExternalsOption: false]],   
        quietOperation: true,   workspaceUpdater:    [$class: 'UpdateWithCleanUpdater']]  
        // Running specific setup functions  
        switch (environment) {   
            case "SIMULATION":    
                script: "bash scripts/setup_simulation.sh"    
                break   
            case "BUILD":    
                script: """python scripts/setup_fpga_synth.py"""    
                break   
            case "BENCH":    
                bat: "python scripts/setup_bench.py"    
                break  
            } 
        } 

stage('GettingStarted') { 
    node( "SIM_1" ) {   
        setup_environment "SIMULATION"   
        // The "validity_check" describe a main set of functionalities that 
        // the RTL must satisfy for being worth building (e.g. registers read and  write)   
        sh "start_simulation.sh validity_check"  
    }  
    node( "BUILDER_1" ) {   
        setup_environment "BUILD"   
        // Run synthesis eventhough where are not sure yet whether the build    
        // will be usable or not   
        script "python scripts/fpga_synthesis.py --target=CHIP"  
    }  
    node( "BENCH_1" ) {   
        setup_environment "BENCH"   
        // Notify the users that the machine will be used for testing.   
        bat: "python scripts/notify_users.py"  
    } 
} 
stage('BuildingAndChecking') {  
    node( "SIM_1" ) {   
        // We survived synthesis and basic simulation, time to stress the    
        // system a bit more   
        sh "scripts/start_simulation.sh full_sim"  
    }  
    node( "BUILDER_1" ) {   
        // Trying to generate valid bitfile from the netlist   
        script """python scripts/fpga_netlist2bitfile.py --target=CHIP"  
    }  
    node( "BENCH_1" ) {   
        // Time to verify if the HW external software are in good condition.   
        // NOTE: We are still running with the previous bitfile. This step is    
        // just to assess the status with everything up to date except the   
        // bitfile (helps in isolating issues)   
        bat: "python run_bench_regression.py --target=CHIP"  
    } 
} 

stage('BuildingAndTesting') {  
    node( "SIM_1" ) {   
        // Cleaning up   
        sh "do_cleanup.sh SIMULATION"  
    }  
    node( "BUILDER_1" ) {   
        // If we are timing clean, just cleaning up, otherwise let's rerun the    
        // netlist2bitfile generation hoping for better results.   
        // Smart scripts can use the previous run failure as a starting point in  
        // search of better initial placement or routing strategy.   
        if (not timing_clean) {    
            script "python fpga_netlist2bitfile.py --target=CHIP --previous_results=LOCATION_OF_PREV_RUN"   
        }   
        sh "do_cleanup.sh BUILD"  
    }  
    node( "BENCH_1" ) {   
        // This time we run with the latest bitfile  and check for problems    
        // even if the bitfile has timing violations (assuming that are minor).   
        // This to identify large issues that would be a waste of build tool effort (e.g. clock not running,    
        // wrong pins)   
        if (not timing_clean) {    
            bat: "python run_bench_regression.py --subset=basic_tests --target=CHIP"   
        } else {    
            // If timing clean let's run all the tests    
            bat: "python run_bench_regression.py --target=CHIP"   
        }  
    } 
}
