#!/usr/bin/env python2.7 

# Copyright (c) 2019, Marco Ghibaudi
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#    This product includes software developed by MARCO GHIBAUDI.

# THIS SOFTWARE IS PROVIDED BY MARCO GHIBAUDI ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL MARCO GHIBAUDI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

import logging def create_header(part, name, extra_entries=None):   
    str = "create_project -force -part " + device + "-name " + project_name   
    # You can add properties, etc. here. 
    # I normally add the configuration options at the very top of the header, instead of 
    # spreading them in multiple Tcl files.   
    if extra_entries :    
        if type(extra_entries) is list:     
            for entry in extra_entries:      
                str = str + "\n" + entry    
            else:     
                str = str + extra_entries   
    return str

 def add_file (fname, library="work", incdir=None):  
    # Taking the extension after the . 
    file_ext = fname.split(".")[-1]  
    if "vhd" in file_ext:   
         # Gets vhd, vhdl extensions   
         str = "read_vhdl " + fname + " -library " + library  
    elif "v" in file_ext:   
        # Gets v, vh, sv extensions   
        str = "read_verilog " + fname  
    elif "xdc" in file_ext:  
        str = "read_xdc " + fname  
    elif "dcp" in file_ext:   
        str = "read_checkpoint " + fname  
    else:   
        logging.error("File: %s not supported", fname)   
        return   
    if incdir:   
        str += " inc_dir= " + incdir   
    return str 

def main():  
    # Main: reads the filelist (always the same for simplicity) and returns a Vivado project .tcl file.  
    # Filelist in csv format: filename (with full path), library (can be left empty), 
    # include directories (can be left empty)  
    with open("output.tcl", 'w') as fout:   
        print >> fout, create_project("VCU190...", "my_build")   
        with open("filelist.csv", 'r') as flist:    
            for entry in flist:     
                # Splitting on "," and removing extra white spaces     
                fname, lib, incdir = entry.split(',').trim()     
                # empty entry for library     
                if not library:      
                    library = "default_library"      
                    print >> fout, add_file(fname, lib, incdir) 

 if __name__ == "__main__":   
     # execute only if run as a script   
     main()