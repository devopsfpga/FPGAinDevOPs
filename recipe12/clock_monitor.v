// Copyright (c) 2019, Marco Ghibaudi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. All advertising materials mentioning features or use of this software
//    must display the following acknowledgement:
//    This product includes software developed by Marco Ghibaudi.
//
// THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

module clock_monitor(
    input clk,
    input ref_clk,
    output reg [15:0] freq_khz 
    );
parameter REF_PERIOD = 10;    

localparam COUNTER_WIDTH = 15;
localparam TICKS_IN_16K = 16000 / REF_PERIOD;

reg [COUNTER_WIDTH-1:0] cnt, ref_cnt = {COUNTER_WIDTH{1'b0}};    
reg toggle_cnt = 1'b0; 
reg toggle_cnt_r = 1'b0;
    
 always @(posedge clk)
      if (toggle_cnt != toggle_cnt_r) begin
           toggle_cnt_r <= toggle_cnt;
           freq_khz <= cnt >> 4;
           cnt <= {COUNTER_WIDTH{1'b0}};
      end
 else
         cnt <= cnt + 1'b1;
   
 always @(posedge ref_clk)
      if (ref_cnt > TICKS_IN_16K)
        begin
         ref_cnt <= {COUNTER_WIDTH{1'b0}};
         toggle_cnt <= !toggle_cnt;
       end
      else
         ref_cnt <= ref_cnt + 1'b1;
             
endmodule
