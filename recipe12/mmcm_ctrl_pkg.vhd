-- Copyright (c) 2019, Marco Ghibaudi
-- All rights reserved.

-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. All advertising materials mentioning features or use of this software
--    must display the following acknowledgement:
--    This product includes software developed by Marco Ghibaudi.

-- THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
-- DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
-- DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
-- LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
-- ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all; 
use ieee.numeric_std.all;
package xilinx_comp_pkg is
  constant ADDR_WIDTH : integer := 7;   
  constant DATA_WIDTH : integer := 16;   
  subtype addr_t is std_logic_vector(ADDR_WIDTH-1 downto 0);   
  subtype data_t is std_logic_vector(DATA_WIDTH-1 downto 0);
  subtype lock_data_t is std_logic_vector(39 downto 0);    
  type addr_array_t is array (0 to 9) of  addr_t;   
  type data_array_t is array (0 to 9) of  data_t;
  type lock_table_t is array (0 to 63) of lock_data_t;     
  constant MMCM_LOCK_COEFF : lock_table_t := (          
    -- RefDly & FBDly & Cnt & SatHigh & UnlckCnt          
  x"31be8FA401",x"31be8FA401",x"423e8FA401",
  x"5afe8FA401",x"73be8FA401",x"8c7e8FA401",
  x"9cfe8FA401",x"b5be8FA401",x"ce7e8FA401",
  x"e73e8FA401",x"fff84FA401",x"fff39FA401", 
  x"ffeeeFA401",x"ffebcFA401",x"ffe8aFA401",
  x"ffe71FA401",x"ffe3fFA401",x"ffe26FA401",
  x"ffe0dFA401",x"ffdf4FA401",x"ffddbFA401",
  x"ffdc2FA401",x"ffda9FA401",x"ffd90FA401", 
  x"ffd90FA401",x"ffd77FA401",x"ffd5eFA401",
  x"ffd5eFA401",x"ffd45FA401",x"ffd45FA401",
  x"ffd2cFA401",x"ffd2cFA401",x"ffd2cFA401",
  x"ffd13FA401",x"ffd13FA401",x"ffd13FA401", 
  x"ffcfaFA401",x"ffcfaFA401",x"ffcfaFA401",
  x"ffcfaFA401",x"ffcfaFA401",x"ffcfaFA401",
  x"ffcfaFA401",x"ffcfaFA401",x"ffcfaFA401",
  x"ffcfaFA401",x"ffcfaFA401",x"ffcfaFA401", 
  x"ffcfaFA401",x"ffcfaFA401",x"ffcfaFA401",
  x"ffcfaFA401",x"ffcfaFA401",x"ffcfaFA401",
  x"ffcfaFA401",x"ffcfaFA401",x"ffcfaFA401",
  x"ffcfaFA401",x"ffcfaFA401",x"ffcfaFA401", 
  x"ffcfaFA401",x"ffcfaFA401",x"ffcfaFA401",
  x"ffcfaFA401");

  -- PWR_REG(x"27"),    
  -- OUTDIV_REG1(x"A"), OUTDIV_REG2(x"B"), DIV_REG(x"16"),   
  -- FDB_REG1(x"14"), FDB_REG2(x"15"),   
  -- LOCK_REG1(x"18"), LOCK_REG2(x"19"), LOCK_REG3(x"1A")   
  -- PWR_REG(x"27")   
  constant ADDRESSES : addr_array_t := (   
    "0100111",      
    "0001010",  "0001011", "0010110",       
    "0010100", "0010101",     
    "0011000", "0011001", "0011010",     
    "0100111");   
  -- Associated masking   
 constant MASKS : data_array_t :=     
  (x"0000",       
   x"1000", x"FC00", x"C000",      
   x"8000", x"1000",         
   x"FC00", x"8000", x"8000",      
   x"0000");
end package xilinx_comp_pkg;	
package body xilinx_comp_pkg is   
end package body;