-- Copyright (c) 2019, Marco Ghibaudi
-- All rights reserved.

-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. All advertising materials mentioning features or use of this software
--    must display the following acknowledgement:
--    This product includes software developed by Marco Ghibaudi.

-- THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
-- DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
-- DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
-- LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
-- ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all; 
use ieee.numeric_std.all; 
library lib_xilinx; 
use lib_xilinx.xilinx_comp_pkg.all;

entity mmcm_ctrl is  
    port ( 
        CLK, RST : in std_logic;   
        N, M, OD : in std_logic_vector(7 downto 0);   
        UPDATE_CONF : in std_logic;   
        DRP_RDY : in std_logic;   
        DRP_EN, DRP_WE : out std_logic;   
        DRP_WE : out std_logic;   
        DRP_DIN : out std_logic_vector(15 downto 0);   
        DRP_ADD : out std_logic_vector(6 downto 0);   
        DRP_DO : in std_logic_vector(15 downto 0);   
        PLL_RESET : out std_logic
); 
end mmcm_ctrl; 

architecture rtl of mmcm_ctrl is 
    -- Signals definition 
    type STATUS_T is (IDLE, READ, ACK_READ, WRITE, ACK_WRITE); 
    signal drp_status : STATUS_T; 
    signal values : data_array_t; 
    signal reg_ctn : data_t; 

    -- Functions 
    function lowcnt(value: std_logic_vector) return std_logic_vector is 
        variable reg : std_logic_vector(5 downto 0); 
    begin  
        reg := std_logic_vector(unsigned(value(6 downto 1)) + unsigned(value(0 downto 0)));  
        return reg; 
    end function lowcnt;

    function dehexify(value: std_logic_vector) return std_logic_vector is 
        variable reg : std_logic_vector(value'range); 
    begin   
        reg := value;   
        -- synthesis translate_off   
        dexifier: for I in value'range loop    
            reg(I) := '0' when value(I) = 'X' else '0' when value(I) = 'U' else value(I);   
        end loop dexifier;  
        -- synthesis translate_on  
        return reg; 
    end function dehexify; 

begin 

    apply_values: process(rst, clk)  
        variable ptr : integer := 0;  
        variable coef : lock_data_t; 
    begin  if (rst = '1') then   
        ptr := 0;   
        drp_status <= IDLE;   
        DRP_ADD <= (others => '0');   
        DRP_EN <= '0';   
        DRP_WE <= '0';   
        PLL_RESET <= '0';  
    elsif (rising_edge(clk)) then   
        DRP_WE <= '0';   
        DRP_EN <= '0';   
        PLL_RESET <= '1';   
        case (drp_status) is    
            -- All the registers accesses will be read-modify-write    
            when IDLE =>     
                PLL_RESET <= '0';     
                ptr := 0;     
                if (UPDATE_CONF = '1') then      
                    -- 0) powerdown  1,2) odiv reg1/2  3) div_reg  4,5) fdb reg1/2       
                    -- 6,7,8) lock reg1/2/3      
                    values(0) <= (others => '1');      
                    values(1) <= "0000" & OD(6 downto 1) & lowcnt(OD);      
                    values(2) <= "00000000" & OD(0) & "0000000";      
                    if (unsigned(N) > 1) then       
                        values(3) <= "00" & N(0) & '0' & N(6 downto 1) & lowcnt(N);      
                    else       
                        values(3) <= x"1041";      
                    end if;      
                    values(4) <= "0001" & M(6 downto 1) & lowcnt(M);      
                    values(5) <= "00000000" & M(0) & "0000000";      
                    coef := MMCM_LOCK_COEFF(64 - to_integer(unsigned(M)));      
                    values(6) <= "000000" & coef(29 downto 20);      
                    values(7) <= '0' & coef(34 downto 30) & coef(9 downto 0);      
                    values(8) <= '0' & coef(39 downto 35) & coef(19 downto 10);      
                    drp_status <= READ;      
                    PLL_RESET <= '1';     
                end if;    
            when READ =>     
                drp_status <= ACK_READ;     
                DRP_EN <= '1';     
                DRP_ADD <= addresses(ptr);    
            when ACK_READ =>     
                if (DRP_RDY = '1') then      
                    drp_status <= WRITE;      
                    reg_ctn <= dehexify(DRP_DO);      
                    if (ptr = 0) then       
                        -- Restoring the initial value to the POWERDOWN register.       
                        values(values'HIGH) <= dehexify(DRP_DO);      
                    end if;     
                end if;    
            when WRITE =>     
                drp_status <= ACK_WRITE;     
                -- drp_addr hasn't changed     
                DRP_WE <= '1';     
                DRP_EN <= '1';     
                DRP_DIN <= (reg_ctn and MASKS(ptr)) or (values(ptr) and (not MASKS(ptr)));    
            when ACK_WRITE =>     
                if (DRP_RDY = '1') then      
                    ptr := ptr + 1;      
                    drp_status <= READ;      
                    if (ptr > values'HIGH) then       
                        drp_status <= IDLE;      
                    end if;     
                end if;   
            end case;  
        end if; 
    end process; 
end rtl; 
 
