// Copyright (c) 2019, Marco Ghibaudi
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. All advertising materials mentioning features or use of this software
//    must display the following acknowledgement:
//    This product includes software developed by Marco Ghibaudi.

// THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

buildsMap=[:]; 
// Define the builders content, Maps are similar to python dictionaries. 
buildsMap["A"]=[when:"@daily", where:"slaveA"]; 
buildsMap["B"]=[when:"@daily", where:"slaveB"]; 
buildsMap["C"]=[when:"@weekly", where:"slaveC"]; 
// Generate the builds 
jobs = [:] 
buildsMap.each { name, config ->  
    def String when  = config.value.when;  
    def String where = config.value.where;  
    def String jobname = "autogen_job_" + name  
    jobs[jobname] = freeStyleJob(name) {   
        description ("Autogenerated from jobSeed")   
        // logRotator(int daysToKeep=-1, int numToKeep=-1, int artifactDaysToKeep=-1,   
        // int artifactNumToKeep=-1)   
        logRotator(10,5,10,3)   
        // Defining when to run the build 
        triggers {    
            // job will run periodically as per content of variable "when"    
            cron (when)   
        }   
        // Defining where to run the build, as instructed by variable "where"   
        label (where)   
        steps {    
            shell("echo Hello World from \${name}!")  
        }  
    } 
}

// Generate an aggregated view 
jobs = [:] listView('ANiceView') {  
    description('All autogenerated jobs')  
    filterBuildQueue()  
    // This shows only the executors used by the listed jobs  
    filterExecutors()  
    jobs {   
        regex('/autogen_job.+/') 
    }  
    jobFilters {   
        // You can add dynamic filters like unstable jobs or failing jobs here.    
        columns {   
            // Adding standard columns to the view   
            status()   
            weather()   
            name()   
            lastDuration()   
            lastSuccess()   
            lastFailure()   
            buildButton()  
        }    
        // List only enabled jobs   
        statusFilter(StatusFilter.ENABLED) 
    }
}

