// Copyright (c) 2019, Marco Ghibaudi
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. All advertising materials mentioning features or use of this software
//    must display the following acknowledgement:
//    This product includes software developed by Marco Ghibaudi.

// THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

stage('SetupSystem'){  
    node('TargetNode'){   
        // Git cloning   
        checkout([$class: 'GitSCM',    
            branches: [[name: '*/master']],    
            doGenerateSubmoduleConfigurations: false,    
            extensions:[], submoduleCfg:[],     
            userRemoteConfigs: [     
                [credentialsId: "#xxx", url: 'xxx.git']]]   
        )   
        // SVN Checkout   
        checkout([$class: 'SubversionSCM',    
            additionalCredentials: [],    
            excludedCommitMessages: '',    
            excludedRegions: '', excludedRevprop: '',    
            excludedUsers: '', filterChangelog: false,    
            ignoreDirPropChanges: false,    
            includedRegions: '',    
            locations: [[     
                cancelProcessOnExternalsFail: true,     
                credentialsId: '',      
                depthOption: 'infinity',     
                ignoreExternalsOption: true,      
                local: 'mysvn',     
                remote: 'http://svn.test']],     
            quietOperation: true,     
            workspaceUpdater:[     
                $class: 'UpdateUpdater']]   
            )   
            dir (mysvn) {    
                // Run from the svn checkout    
                sh "python run_test.py"   
            }   
            // Store all the results excluding python compiled files   
            archiveArtifacts artifacts: 'output/*.log', excludes: 'output/*.pyc',  fingerprint: true   
            // Check whether the build has failed   
            if (currentBuild.currentResult != "SUCCESS") {    
                // Build has failed, email the users    
                emailext body: '${JELLY_SCRIPT, template="html"}', 
                    subject: '$PROJECT_NAME- Build  # $BUILD_NUM', to: "user@mail"   
            } else {    
                // Parses the Junit test results    
                junit 'output/my_junit.xml'   
            }  
        } 
    }