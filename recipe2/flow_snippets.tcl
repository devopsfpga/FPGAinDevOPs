# Copyright (c) 2019, Marco Ghibaudi
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#    This product includes software developed by Marco Ghibaudi.

# THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Unassigned I/O Standard 
# Unconstrained Logical Port 
# Combinatorial loops 
set_property SEVERITY Fatal \ [get_drc_checks {LUTLP-1 UCIO-1 LUTLP-1}]

# Disable timing checks on synthesis 
synth_design -no_timing_driven [your_opts] 

# Reduce severity 
set_msg_config -id {Synth 8-32} \ -new_severity "INFO" 
# Waive completely the error 
set_msg_config -id {Synth 8-32} \ -suppress -string {unique portion \ of the message}

# Add extra DRC checks 
report_drc -name drc_1 -ruledecks {\ default opt_checks placer_checks \ router_checks bitstream_checks}

# Improve the timing analysis accuracy 
set_delay_model -interconnect estimated

# Retrieve the largest violations  (setup and hold) 
set slack [get_property SLACK [get_timing_paths]] 
# Check the slack and fail if too large 
if {[$slack < -10.0]} {    
    puts {Timing violation too large, stopping }    
    exit
}

# Iterate place phase 
set place_iter 0 
place_design 
if{[$slack < -10.0]}{ 
    # Desperate situation. Better notify the designer and exit  
    notify_owner  
    exit 
} elseif {[$slack < -5.0]}{  
    # Will run at the most two extra times...  
    if {[$place_iter < 2]} {    
        set place_iter {expr $place_iter + 1}    
        place_design -unplace    
        # Run place design and try one more time with different strategy    
        place_design [different_strategy]  
    } else {    
        # Strike three...    
        notify_owner    
        exit  
    } 
} else {  
    if{[$slack < 0.0]}{   
        # design with less than 5ns total slack, worth trying to run  physical 
        # optimization on it   
        phys_opt_design  
    } else {    
        puts "Timing clean" 
    }
}
route_design
