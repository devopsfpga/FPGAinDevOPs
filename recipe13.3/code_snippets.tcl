# Copyright (c) 2019, Marco Ghibaudi
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#    This product includes software developed by Marco Ghibaudi.

# THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Set your IP folder path 
set IP_FOLDER "YOUR_FOLDER_PATH" 
set IP_NAME FIFO_dual_clock_block_A 
create_project ip_project $IP_FOLDER/ip_project -part xc7k70tfbv676-1 -ip -force 
# Creating the IP and setting the options 
create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name $IP_NAME -dir $IP_FOLDER 
set_property -dict  [list CONFIG.Fifo_Implementation {Independent_Clocks_Block_RAM} \  
CONFIG.Almost_Full_Flag {true} CONFIG.Input_Data_Width{32}  CONFIG.Input_Depth{512}  CONFIG.Output_Data_Width{32} \  CONFIG.Output_Depth{512}  CONFIG.Almost_Empty_Flag{true}  CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Full_Flags_Reset_Value{1}\  CONFIG.Full_Threshold_Assert_Value{509}  CONFIG.Full_Threshold_Negate_Value{508}]  [get_ips $IP_NAME] 
# Generating targets and exporting files 
generate_target all [get_files ${IP_FOLDER}/$IP_NAME/$IP_NAME.xci] 
export_ip_user_files -of_objects  [get_files $IP_FOLDER/$IP_NAME/$IP_NAME.xci] -no_script -sync -force -quiet 
create_ip_run [get_files -of_objects [get_fileset sources_1] $IP_FOLDER/$IP_NAME/$IP_NAME.xci] 
launch_runs -jobs 4 $IP_NAME_synth_1 
# Export simulation files (can be reduced) 
set CACHEPATH ip_project/ip_project.cache 
set SIMPATH $CACHEPATH/compile_simlib 
set IP_USERFILES $IP_FOLDER/ip_user_files 
export_simulation -of_objects [get_files  $IP_FOLDER/$IP_NAME/$IP_NAME.xci] -directory $IP_USERFILES/sim_scripts \  -ip_user_files_dir $IP_USERFILES -ipstatic_source_dir $IP_USERFILES/ipstatic -lib_map_path [list \  {modelsim=$IP_FOLDER/$SIMPATH/modelsim} {questa=$IP_FOLDER/$SIMPATH/questa} {riviera=$IP_FOLDER/$SIMPATH/riviera} \  {activehdl=$IP_FOLDER/$SIMPATH/activehdl}] -use_ip_compiled_libs -force -quiet