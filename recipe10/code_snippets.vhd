-- Copyright (c) 2019, Marco Ghibaudi
-- All rights reserved.

-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. All advertising materials mentioning features or use of this software
--    must display the following acknowledgement:
--    This product includes software developed by Marco Ghibaudi.

-- THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
-- DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
-- DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
-- LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
-- ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

library IEEE; 
use IEEE.STD_LOGIC_1164.ALL; 

entity I2C_scl_gen is  
    generic ( 
        SYS_FREQ_MHZ : integer 
    );  
    port (
        rst, clk : in std_logic;        
        scl : out std_logic
    ); 
end I2C_scl_gen; 

architecture rtl of I2C_scl_gen is  
    signal scl_i : std_Logic; 
    
    begin  process (rst, clk)   
        variable cnt : integer := 0;  
    begin   
        if (rst = '1') then    
            scl_i <= '0';    
            cnt := 0;   
        elsif (rising_edge(clk)) then    
            cnt := cnt + 1;    
            -- Toggling every 1/20 us (freq = 100 Khz)    
            if (cnt = SYS_FREQ_MHZ/(10*2)) then     
                scl_i <= not scl_i;     
                cnt := 0;    
            end if;   
        end if;
    end process;  
    -- Assigning output  
    scl <= scl_i; 
end rtl;



-------------------------------------------------------------------------------- 
-- This component is used to drive the I2C sda and scl signals via VIOs 
-------------------------------------------------------------------------------- 

library IEEE; 
use IEEE.std_logic_1164.all; 
use IEEE.numeric_std.all; 
use IEEE.std_logic_unsigned.all;

entity I2C_driver is  
    generic ( 
        SYS_FREQ_MHZ : integer := 100000 
    );  
    port (
        rst, sys_clk : in std_logic;        
        sda : inout std_logic;        
        scl : out std_logic
    ); 
end I2C_driver; 

architecture rtl of I2C_driver is 
    -- signals definition  
    signal sda_out, sda_in : std_logic;  
    signal sda_in_w : std_logic_vector(7 downto 0);  
    signal scl_i, sda_dir : std_logic;  
    signal wr_nrd, strobe : std_logic;  
    signal scl_enb : std_logic;  
    signal data, slave_address, reg_address, err_cnt : std_logic_vector(7 downto 0);  
    signal address_rwn: std_logic_vector(8 downto 0); 
    -- FSM status  
    type FSM_STATUS_TYPE is (IDLE_PHASE, ADDRESS_PHASE, ACK_PHASE_1,    
        FRAME_DATA_PHASE, ACK_PHASE_2, FRAME_WR_DATA_PHASE, FRAME_RD_DATA_PHASE,   
        ACK_PHASE_3, ERR_PHASE);  
    signal op : FSM_STATUS_TYPE;  
    subtype status_t is std_logic_vector(1 downto 0);  
    signal op_status : status_t;  constant IDLE : status_t := "00";  
    constant RUNNING : status_t := "01";  
    constant ERROR : status_t := "11";  
    constant SUCCESS : status_t := "10";

 -- components definition 
 COMPONENT vio_0 PORT (  
    clk : IN STD_LOGIC;  
    probe_in0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);  
    probe_in1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);  
    probe_in2 : IN STD_LOGIC_VECTOR(1 DOWNTO 0);  
    probe_out0 : OUT STD_LOGIC_VECTOR(7 DOWNTO  0);  
    probe_out1 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);  
    probe_out2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);  
    probe_out3 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);  
    probe_out4 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)); 
END COMPONENT; 

component I2C_scl_gen is  
    generic (
        SYS_FREQ_MHZ : integer
    ); 
    port (
        rst, clk : in std_logic;        
        scl : out std_logic); 
end component; 

begin 
    -- NOTE: The address will contain the START BIT as well. 
    u_VIO : vio_0 port map (
            clk => sys_clk,  
            probe_out0 => slave_address,  
            probe_out1 => reg_address,  
            probe_out2 => data,  
            probe_out3(0) => wr_nrd,  
            probe_out4(0) => strobe,  
            probe_in0 => sda_in_w,  
            probe_in1 => err_cnt,  
            probe_in2 => op_status
    );

    u_I2C_scl_gen : I2C_scl_gen 
        generic map (SYS_FREQ_MHZ=> SYS_FREQ_MHZ) 
        port map (
            rst => rst,  
            clk => sys_clk,  
            scl => scl_i
        );

    process (scl_i, rst)  
        variable cnt : integer; 
    begin  
        if (rst = '1') then   
            sda_out <= '0';   
            sda_dir <= '0';   
            scl_enb <= '0';   
            op <= IDLE_PHASE;   
            op_status <= IDLE;  
        elsif (rising_edge(scl_i)) then   
            case (op) is    
                when IDLE_PHASE =>     
                    if (strobe = '1') then      
                        op_status <= RUNNING;      
                        op <= ADDRESS_PHASE;      
                        sda_dir <= '1';      
                        -- Concatening the slave address to the field read/not write      
                        address_rwn <= slave_address & wr_nrd;     
                    end if;     
                    cnt := 0;    
                when ADDRESS_PHASE =>     
                    -- Handling only 7 bits addressing here. Last bit to be sent in      
                    -- this phase is the Read/Write bit     
                    if (cnt = 8) then      
                        op <= ACK_PHASE_1;      
                        sda_dir <= '0';      
                    end if;     
                    sda_out <= address_rwn(8-cnt);     
                    scl_enb <= '1';     
                    cnt := cnt + 1;    
                when ACK_PHASE_1 =>     
                    -- ACK condition     
                    if sda_in = '0' then      
                        op <= FRAME_DATA_PHASE;     
                    else -- NAK condition      
                        op <= ERR_PHASE;     
                    end if;     
                    cnt := 0;    
                when FRAME_DATA_PHASE =>     
                    if (cnt = 7) then      
                        op <= ACK_PHASE_2;       
                        sda_dir <= '0';     
                    end if;     
                    sda_out <= reg_address(7-cnt);     
                    scl_enb <= '1';     
                    cnt := cnt + 1;    
                when ACK_PHASE_2 =>     
                    -- ACK condition     
                    if sda_in = '0' then      
                        if (wr_nrd = '1') then       
                            op <= FRAME_WR_DATA_PHASE;       
                            sda_dir <= '1';      
                        else       
                            op <= FRAME_RD_DATA_PHASE;       
                            sda_dir <= '0';      
                        end if;     
                    else 
                        -- NAK condition      
                        op <= ERR_PHASE;     
                    end if;     
                    cnt := 0;    
                when FRAME_WR_DATA_PHASE =>     
                    if (cnt = 7) then      
                        op <= ACK_PHASE_3;     
                    end if;     
                    sda_out <= reg_address(7-cnt);     
                    scl_enb <= '1';     
                    cnt := cnt + 1;     
                    sda_dir <= '0';    
                when FRAME_RD_DATA_PHASE =>     
                    if (cnt = 7) then      
                        op <= ACK_PHASE_3;     
                    end if;     
                    sda_in_w(7-cnt) <= sda_in;     
                    scl_enb <= '1';     
                    cnt := cnt + 1;     
                    sda_dir <= '1';    
                when ACK_PHASE_3 =>     
                    -- ACK condition     
                    if sda_in = '0' then      
                        op <= IDLE_PHASE;      
                        op_status <= SUCCESS;     
                    else 
                        -- NAK condition      
                        op <= ERR_PHASE;        
                        op_status <= ERROR;     
                    end if;     
                    cnt := 0;    
                when ERR_PHASE =>     
                    -- Just counting the errors, Returning to the IDLE state     
                    err_cnt <= std_logic_vector(unsigned(err_cnt) + 1);     
                    op <= IDLE_PHASE;   
            end case;  
        end if; 
    end process;

    -- Generate external signal and store data  
    scl <= not scl_i; 
    -- Reverse polarity  
    sda <= sda_out when sda_dir='1' else 'Z';  
    sda_in <= sda; 
end RTL; 


