# Copyright (c) 2019, Marco Ghibaudi
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#    This product includes software developed by Marco Ghibaudi.

# THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

##------------------------------------------------------------------------------  
## This component is used to drive the I2C sda and scl signals via VIOs 
##------------------------------------------------------------------------------  
proc i2c_connect {} {  
    open_hw  
    connect_hw_server  
    current_hw_target [get_hw_targets */xilinx_tcf/Digilent/*]  
    open_hw_target  
    refresh_hw_device [get_hw_devices]  
    # Assuming only one VIO  
    set_property CORE_REFRESH_RATE_MS 1000 [get_hw_vios hw_vio_1] 
} 

proc set_address {dev_address reg_address} {  
    set_property OUTPUT_VALUE $dev_address [get_hw_probes slave_address]  
    set_property OUTPUT_VALUE $reg_address [get_hw_probes reg_address]  
    commit_hw_vio [get_hw_vios hw_vio_1] 
} 

proc do_cmd {} {  
    set_property OUTPUT_VALUE 1 [get_hw_probes strobe]  
    commit_hw_vio [get_hw_vios hw_vio_1]  
    # Clearing the new command flag  
    set_property OUTPUT_VALUE 0  [get_hw_probes strobe]  
    commit_hw_vio [get_hw_vios hw_vio_1] 
} 

proc check_status {} {  
    set SUCCESS 10  set status [get_property INPUT_VALUE [get_hw_probes op_status]]  
    set address [get_property OUTPUT_VALUE [get_hw_probes reg_address]]  
    if { $status != $SUCCESS } {   
        puts "ERROR: $status"   
        puts "Write at addr: $address"  
    } 
}

proc set_cmd {cmd} {  
    if { $cmd == "write"} {   
        set_property OUTPUT_VALUE 1 [get_hw_probes wr_nrd]  
    } elseif { $cmd == "read" } {   
        set_property OUTPUT_VALUE 0 [get_hw_probes wr_nrd]  
    }  
    commit_hw_vio [get_hw_vios hw_vio_1]  
} 

proc write_word {dev_address reg_address word} {  
    # Using our procs
    set_address $dev_address $reg_address  
    set_property OUTPUT_VALUE $word [get_hw_probes data]  
    commit_hw_vio [get_hw_vios hw_vio_1]  
    set_cmd "write"  
    do_cmd  
    check_status 
} 

proc read_word {dev_address reg_address} {  
    # Using our procs  
    set_address $dev_address $reg_address  
    set_cmd "read"  
    do_cmd  
    check_status  
    set rback [get_property INPUT_VALUE [get_hw_probes sda_in_w]]  
    return $rback 
}