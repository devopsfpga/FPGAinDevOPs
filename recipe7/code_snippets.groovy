// Copyright (c) 2019, Marco Ghibaudi
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. All advertising materials mentioning features or use of this software
//    must display the following acknowledgement:
//    This product includes software developed by Marco Ghibaudi.

// THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

stage('Stage:1'){  
    node("BuildServer"){   
        checkout([$class: 'SubversionSCM', ...)    
        dir(checkout_folder) {     
            sh 'python generate_project_files.py'     
            stash 'synth.tcl'     
            stash 'synth_synplify.tcl'     
            stash 'run_reports.tcl'     
            stash 'place.tcl'     
            stash 'route.tcl'    
        }   
    }  
} 
parallel {  
    stage('Stage:2a-3'){   
        node("BuildServer"){    
            sh """vivado -mode batch synth.tcl"""    
            stash post_synth.dcp   
        }  
    }  
    stage('Stage:2b-3'){   
        node("BuildServer"){    
            // The Synplify tcl has to generate a dcp for Vivado.    
            sh """synplify -mode batch -source synth_synplify.tcl"""    
            stash post_synth_synplify.dcp    
        }  
    }  
    stage('Stage:2c'){   
        node("BuildServer"){    
            // Fails faster than the other builds    
            sh """vivado -mode batch synth.tcl -tclargs quick"""   
        }  
    } 
}
stage('Stage:4'){  
    node("BuildServer"){   
        // retrieving the checkpoints   
        unstash post_synth.dcp   
        unstash post_synth_synplify.dcp   
        // Inside the place script you can implement the logic to select   
        // synplify or vivado build   
        sh vivado -mode batch 'place.tcl'   
        stash post_place.dcp  
    } 
}
parallel {  
    stage('Stage:4a-7'){   
        node("BuildServer"){    
            // retrieving the checkpoints    
            unstash post_synth.dcp   
            unstash post_synth_synplify.dcp    
            // Inside the place script  you can implement the     
            // logic to select synplify or vivado build      
            sh vivado -mode batch 'place.tcl'    
            stash post_place.dcp   
        }  
    }  
    stage('Stage:4b'){   
        node("BuildServer"){    
            // retrieving the checkpoints    
            unstash post_synth.dcp    
            // Synth reports    
            sh """vivado -mode batch reports.tcl -tclargs 'synth'"""    
            stash *.log  
        }  
    } 
}


parallel {  
    stage('Stage:11a'){   
        node("BuildServer"){    
            unstash post_route.dcp    
            sh """vivado -mode batch -source optimize.tcl -tclargs post_route"""    
            sh """vivado -mode batch -source gen_bitfile.tcl"""    
            artifact post_route_opt.dcp   
        }  
    }  
    stage('Stage:11b'){   
        node("BuildServer"){    
            unstash post_route.dcp    
            sh """vivado -mode batch -source 'reports.tcl' -tclargs 'synth'"""    
            artifact *.log  
        }
    }  
    stage('Stage:11c'){   
        node("BuildServer"){    
            unstash post_route.dcp    
            sh """vivado -mode batch -source gen_bitfile.tcl"""   
        }  
    } 
}


