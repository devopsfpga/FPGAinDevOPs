// Copyright (c) 2019, Marco Ghibaudi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. All advertising materials mentioning features or use of this software
//    must display the following acknowledgement:
//    This product includes software developed by Marco Ghibaudi.
//
// THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

`timescale 1ns / 1ps 
module simple_dual_port_ram #(  
    parameter RWIDTH = 256,  
    parameter RDEPTH = 32,   
    parameter RDEPTHLOG2 = 5,  
    parameter XGENERATION = "NO-X")
( 
    input wire [RDEPTHLOG2-1:0] addra,  
    input wire [RWIDTH-1:0] dina,  
    input wire clka, ena, wea, clkb, enb,  
    input wire [RDEPTHLOG2-1:0] addrb,   
    output wire [RWIDTH-1:0] doutb
); 

// Define a symbol to distinguish sim/synth 
`define SYNTH 
// synthesis-translate off 
`undef SYNTH 
// synthesis-translate on 

reg [RWIDTH-1:0] row; 
// Memory definition 
(*ram_style="block"*) reg [RWIDTH-1:0] mem [(2**RDEPTHLOG2)-1:0]; 
always @(posedge clka)  
    if (ena)   
        if (wea)    
            mem[addra] <= dina;   

always @(posedge clkb)  
    if (enb) begin   
        row <= mem[addrb];  
    end

generate 
    `ifdef SYNTH  
        assign doutb = row; 
    `else   
        if (XGENERATION == "NO-X")    
            assign doutb = row;   
        else    
            // Conditional assignment. X generated     
            // when simultaneous write and read  to the same location    
            assign doutb = ((addra == addrb) & (wea &  enb == 1'b1)) ? row : {(RWIDTH){1'bX}};  
    `endif 
endgenerate 
endmodule 
