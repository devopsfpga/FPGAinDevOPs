#!/usr/bin/env python2.7 

# Copyright (c) 2019, Marco Ghibaudi
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#    This product includes software developed by MARCO GHIBAUDI.

# THIS SOFTWARE IS PROVIDED BY MARCO GHIBAUDI ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL MARCO GHIBAUDI BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

import argparse 
import subprocess 
import logging 
import random 

# Constants 
MAX_VAL = 1200 
MIN_VAL = 500 
TOLERANCE = 1 
XTAL_FREQ = 100 
# useful registers 
ADDR_CONF_REG0 = 0x200 
ADDR_CONF_REG2 = 0x208 
ADDR_CONF_REG23 = 0x25c

def _write_reg(addr, value):  
    """ Non-Xilinx debug busses should/can use a similar approach, just by changing this low level function """  
    try:   
        subprocess.check_output(["vivado", "-mode", "batch", "-source", "jtag_driver.tcl", "-nolog", 
            "-tclargs=0x%04x" % (addr), "-tclargs=0x%08x" % (value)], shell=True)   
        with open("result.txt") as fr:    
            assert "OK" in fr.readline(),     
                "Operation failed"  
    except subprocess.CalledProcessError, e:   
        print e.output 

 def _read_reg(addr):  
     """ Non-Xilinx debug busses should/can   use a similar approach, just by changing  this low level function """  
     value = None  
     try:   
         msg = subprocess.check_output(["vivado", "-mode", "batch", "-source", "jtag_driver.tcl", "-tclargs=0x%04x" % addr],
             stderr=subprocess.STDOUT, shell=True)   
        with open("result.txt") as fr:    
            value = int(fr.readline(),16)  
    except subprocess.CalledProcessError, e:   
        print e.output  
    return value

 def set_pll_frequency(value): 
    """ Finds and sets the frequency of the PLL (only one output)  Simplified version, only one output clock supported """  
    main_div = 1  
    main_mult = MIN_VAL / XTAL_FREQ  
    out_div = (XTAL_FREQ * main_mult) / value  
    comp_freq = (XTAL_FREQ * main_mult) / (main_ div * out_div * 1.0)  
    while True:   
        loopback_freq = (XTAL_FREQ * main_mult) / (main_div * 1.0)   
        logging.debug("main_div: %d, main_mult: %d, out_div: %d, comp_freq: %d, loopback_freq: %d"     
            % (main_div, main_mult, out_div, comp_freq, loopback_freq))   
        if (loopback_freq < MIN_VAL):    
            if main_mult < 16:     
                main_mult = main_mult + 1    
            else:     
                main_div = main_div - 1    
            continue    
        if (loopback_freq > MAX_VAL):    
            if main_div < 16:      
                main_div = main_div - 1     
            else :      
                main_mult = main_mult + 1     
            continue   
        comp_freq = (XTAL_FREQ * main_mult) / (main_div * out_div *1.0)   
        if abs(comp_freq - value) < 1:    
            if ((loopback_freq >= MIN_VAL) and (loopback_freq <= MAX_VAL)):     
                break    
            else:     
                continue   
        else:    
            if comp_freq > value:     
                # Use randomization to avoid getting stuck in a never-ending loop    
                out_div = out_div + int(random.getrandbits(1))    
                main_mult = main_mult - int(random.getrandbits(1))   
            else:    
                out_div = out_div - int(random.getrandbits(1))    
                main_mult = main_mult +  int(random.getrandbits(1))   
            print "Configuration found: " +  "achieved frequency - %d" %  comp_freq   
            conf_reg0 = main_mult * 256 + main_div   
            _write_reg(ADDR_CONF_REG0, conf_reg0)   
            conf_reg2 = out_div   
            _write_reg(ADDR_CONF_REG2, conf_reg2)   
            _write_reg(ADDR_CONF_REG23, 0x00000003) 

 def get_pll_frequency():  
    conf_reg0 = _read_reg(ADDR_CONF_REG0)   
    main_div = conf_reg0 & 0xFF  
    main_mult = (conf_reg0 >> 8) & 0xFF  
    conf_reg2 = _read_reg(ADDR_CONF_REG2)  
    out_div = conf_reg2 & 0xFF  
    freq = XTAL_FREQ * main_mult / (main_div * out_div)  
    print "Freq=%d MHz" % freq  
    logging.debug("main_div=%d, main_mult=%d, out_div=%d" %  
        (main_div, main_mult, out_div))

def program_fpga():  
    try:   
        msg = subprocess.check_output(["vivado", "-mode", "batch", "-source", "program_device.tcl"],   
            stderr=subprocess.STDOUT, shell=True)  
    except subprocess.CalledProcessError, e:   
        print e.output 

def main():  
    parser = argparse.ArgumentParser()  
    group = parser.add_mutually_exclusive_group()  
    group.add_argument("--program", action="store_true", help="Program the FPGA")  
    group.add_argument("--set_freq", type=int, help="Sets the freq. of the output clock")  
    group.add_argument("--get_freq", action="store_true", help="Gets the freq. of the output clock")  
    parser.add_argument("--debug", action="store_true", help="Enables debugger")  
    args = parser.parse_args()  
    if args.debug:   
        logger = logging.getLogger('')   
        logger.setLevel(logging.DEBUG)  
        if args.set_freq:   
            set_pll_frequency(args.set_freq)  
        if args.get_freq:   
            get_pll_frequency()  
        if args.program:   
            program_fpga()

if __name__ == "__main__":  
    main()
