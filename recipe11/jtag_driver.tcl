# Copyright (c) 2019, Marco Ghibaudi
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#    This product includes software developed by Marco Ghibaudi.

# THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


## Usage: performs basic reads and writes via jtag-axi bridge 
proc jtag_write_reg {addr value} {  
    create_hw_axi_txn -address $addr -type write -data $value 
    tmp [get_hw_axis hw_axi_1] -force  
    run_hw_axi -quiet [get_hw_axi_txns] 
} 

proc jtag_read_reg { addr } {  
    create_hw_axi_txn -address $addr -type read rd_txn [get_hw_axis hw_axi_1]  
    run_hw_axi -quiet [get_hw_axi_txns]  
    set rd_report [report_hw_axi_txn rd_txn -w 8]  
    lassign [regexp -all -inline  {[0-9a-f]+} $rd_report]  raddr rdata  return $rdata 
}

# "Main" equivalent 
set addr [lindex $argv 0] 
set mode "read" 
puts "Number of arguments: $argc" puts "Addr: $addr" 
if { $argc == 2 } {  
    set data [lindex $argv 1]  
    set mode "write"  
    puts "Data: $data" 
} 
# Openining link... 
open_hw 
connect_hw_server 
current_hw_target [get_hw_targets */xilinx_tcf/Digilent/*] 
open_hw_target refresh_hw_device [get_hw_devices] 
if { "$mode" == "read" } {  
    set status [jtag_read_reg $addr]  
    set label "JTAG_READ" 
} if { "$mode" == "write" } {  
    puts "Starting write operation"   
    jtag_write_reg $addr $data  
    set label "JTAG_WRITE"  
    set status "OK" 
} 
set rfile [open "result.txt" w] 
puts $rfile "$status" 
close $rfile 
