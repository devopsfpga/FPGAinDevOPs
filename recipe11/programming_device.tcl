# Copyright (c) 2019, Marco Ghibaudi
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must display the following acknowledgement:
#    This product includes software developed by Marco Ghibaudi.

# THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Usage: programs the FPGA with a predefined bitfile 
open_hw 
connect_hw_server 
open_hw_target 
# Setting up bitfile and probe 
set_property PROGRAM.FILE {x/prog_clocks.bit}  [get_hw_devices xc7a35t_0] 
set_property PROBES.FILE {x/prog_clocks.ltx} [get_hw_devices xc7a35t_0] 
set_property FULL_PROBES.FILE {x/prog_clocks.ltx} [get_hw_devices xc7a35t_0] 
# Program device current_hw_device [get_hw_devices xc7a35t_0] 
refresh_hw_device [lindex [get_hw_devices xc7a35t_0] 0] 
program_hw_devices [get_hw_devices xc7a35t_0] 
refresh_hw_device [lindex [get_hw_devices xc7a35t_0] 0]