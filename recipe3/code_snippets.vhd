-- Copyright (c) 2019, Marco Ghibaudi
-- All rights reserved.

-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. All advertising materials mentioning features or use of this software
--    must display the following acknowledgement:
--    This product includes software developed by Marco Ghibaudi.

-- THIS SOFTWARE IS PROVIDED BY Marco Ghibaudi ''AS IS'' AND ANY
-- EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
-- DISCLAIMED. IN NO EVENT SHALL Marco Ghibaudi BE LIABLE FOR ANY
-- DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
-- LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
-- ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-- Signals definition and MARK_DEBUG attribute 
signal err : std_logic_vector(1 downto 0); 
ATTRIBUTE MARK_DEBUG : string; 
ATTRIBUTE MARK_DEBUG of err: SIGNAL IS "TRUE"; 

-- Groups the status signals to simplify ILA 
debug_err: process (clk) 
begin 
    if (rising_edge(clk)) then   
        err(1) <= fifo_full and write;   
        err(0) <= read and crc_error; 
    end if; 
end process; 



-- Generate a regular toggling for the LED 
process(rst, clk_A)
  variable cnt : integer; 
begin 
    if (rst='1') then   
        LED <= '0';   
        cnt := 0; 
    elsif (rising_edge(clk_A)) then   
        cnt := cnt + 1;   
        -- MHz to KHz conversion.   
        if (cnt = 1000) then      
            cnt := 0;      
            LED <= not LED;    
        end if; 
    end if;
end process;
